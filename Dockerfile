FROM composer:1.9.0 AS composer
WORKDIR /app
COPY composer.json .
COPY composer.lock .
RUN composer install --no-scripts

###

FROM node:12.9.1-slim AS node
WORKDIR /app
COPY package.json .
COPY yarn.lock .
COPY webpack.config.js .
COPY ./assets ./assets
RUN yarn && mkdir public && yarn encore dev

###

FROM php:7.2-apache
WORKDIR /var/www/project
COPY --chown=www-data:www-data . /var/www/project
COPY --chown=www-data:www-data --from=composer /app/vendor /var/www/project/vendor
COPY --chown=www-data:www-data --from=node /app/public/build /var/www/project/public/build
RUN sed -i "s|DocumentRoot /var/www/html|DocumentRoot /var/www/project/public|" /etc/apache2/sites-available/000-default.conf && \
    a2enmod rewrite